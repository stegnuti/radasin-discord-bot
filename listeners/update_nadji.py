from listeners.listener import Listener


class NadjiOptIn(Listener):
    async def notify(self, data, store):
        if "reaction" not in data.keys():
            return False
        if data["reaction"].message.id not in store["nadji"].keys():
            return False

        message = data["reaction"].message
        nadji_post = store["nadji"][message.id]

        if nadji_post.satisfied():
            await data["reaction"].remove(data["user"])
            await data["user"].send("Nema više mesta u tom 'nadji'!")
            return True

        await nadji_post.on_add_react(data["reaction"], data["user"])
        await nadji_post.author.send(f"Opa! <@!{data['user'].id}> ti se prijavio na !nadji.")
        return True

class NadjiOptOut(Listener):
    async def notify(self, data, store):
        if "message" not in data.keys():
            return False
        if data["message"].id not in store["nadji"].keys():
            return False

        await store["nadji"][data["message"].id].on_remove_react(data["message"])
        return True